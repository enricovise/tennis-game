import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec

class GameSpec extends AnyFunSpec with GivenWhenThen {

  describe("A game") {
    it("should have a score") {
      Given("a new game starts")
      val game00 = new Game

      Then("score is love-love")
      assert(game00.score == "love-love")

      When("west side wins the ball")
      val game01 = game00.toWest
      Then("score is fifteen-love")
      assert(game01.score == "fifteen-love")

      When("west side wins another ball")
      val game02 = game01.toWest
      Then("score is thirty-love")
      assert(game02.score == "thirty-love")

      When("west side wins the third ball")
      val game03 = game02.toWest
      Then("score is forty-love")
      assert(game03.score == "forty-love")

      When("east side wins the fourth ball")
      val game04 = game03.toEast
      Then("score is forty-fifteen")
      assert(game04.score == "forty-fifteen")

      When("east side wins the fifth ball")
      val game05 = game04.toEast
      Then("score is forty-thirty")
      assert(game05.score == "forty-thirty")

      When("east side wins the sixth ball")
      val game06 = game05.toEast
      Then("score is deuce")
      assert(game06.score == "deuce")

      When("west side wins the seventh ball")
      val game07 = game06.toWest
      Then("score is advantage West")
      assert(game07.score == "advantage West")

      When("east side wins the eighth ball")
      val game08 = game07.toEast
      Then("score is deuce")
      assert(game08.score == "deuce")

      When("east side wins the ninth ball")
      val game09 = game08.toEast
      Then("score is advantage East")
      assert(game09.score == "advantage East")

      When("east side wins the tenth ball")
      val game10 = game09.toEast
      Then("score is East won")
      assert(game10.score == "East won")
    }
  }
}
