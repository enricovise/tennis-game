import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec

class ScoreSpec extends AnyFunSpec with GivenWhenThen {

  describe("A side") {
    it("should have a score") {
      Given("the side is created")
      val side00 = new Score
      Then("its fancy score is love")
      assert(side00.getCall == "love")

      When("the side wins a ball")
      val side01 = side00.oneUp
      Then("its fancy score is fifteen")
      assert(side01.getCall == "fifteen")

      When("the side wins another ball")
      val side02 = side01.oneUp
      Then("its fancy score is thirty")
      assert(side02.getCall == "thirty")

      When("the side wins one more ball")
      val side03 = side02.oneUp
      Then("its fancy score is forty")
      assert(side03.getCall == "forty")
    }

    it("should be comparable to another side with respect to their scores") {
      Given("a small side")
      val smallSide = new Score
      And("a big side")
      val bigSide = Score(1)
      Then("the big side is greater than the small side")
      assert(smallSide < bigSide)
    }
  }
}
