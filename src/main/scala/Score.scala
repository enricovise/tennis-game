case class Score(magnitude: Int = 0) extends Ordered[Score] {

  def getCall: String = this.magnitude match {
    case 0 => "love"
    case 1 => "fifteen"
    case 2 => "thirty"
    case 3 => "forty"
  }

  def oneUp: Score = Score(this.magnitude + 1)

  def compare(that: Score): Int = this.magnitude - that.magnitude

  def isHigh: Boolean = this.magnitude > 2
}

