class Game(west: Score = new Score, east: Score = new Score) {

  def score: String = {
    if (this.west == this.east && this.west.isHigh) {
      "deuce"
    }
    else if (this.west > this.east && this.east.isHigh) {
      if (this.west > this.east.oneUp) "West won" else "advantage West"
    }
    else if (this.east > this.west && this.west.isHigh) {
      if (this.east > this.west.oneUp) "East won" else "advantage East"
    }
    else {
      s"${this.west.getCall}-${this.east.getCall}"
    }
  }

  def toWest: Game = new Game(this.west.oneUp, this.east)

  def toEast: Game = new Game(this.west, this.east.oneUp)
}
